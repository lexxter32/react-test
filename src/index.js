import './bootstrap.min.css';
import './style.css';

import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware} from 'redux'
import reducers from 'reducers'
import thunk from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension'
import {browserHistory} from 'react-router'
import  {syncHistoryWithStore} from 'react-router-redux'
import  {Provider} from 'react-redux'
import {Router, Route} from 'react-router'

import Users from 'containers/users'


// import registerServiceWorker from './registerServiceWorker';
export const store =createStore(reducers, composeWithDevTools(
    applyMiddleware(thunk)
));
const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
    <Provider store={store}>
        <Router history={history} >
            <Route path='/' component={Users}/>
        </Router>
    </Provider>,
    document.getElementById('root')
);
